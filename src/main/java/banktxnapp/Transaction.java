package banktxnapp;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

@Entity
@Table(name="xferaccount")
public class Transaction {


	private Long id;
	private Long acFrom;
	private Long acTo;
	private double txnAmount;
	private char status;
	private Timestamp txnCreated;
	private Timestamp txnUpdated;
	
	public Transaction() {
	}

	public Transaction(Long id, Long acFrom, Long acTo, double txnAmount,
			char status, long txnCreated, long txnUpdated) {
		super();
		this.id = id;
		this.acFrom = acFrom;
		this.acTo = acTo;
		this.txnAmount = txnAmount;
		this.status = status;
		this.txnCreated = new Timestamp(txnCreated);
		this.txnUpdated = new Timestamp(txnUpdated);
	}

	@Id
	@Column(name ="txnid",unique=true,nullable=false)
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name="from_acc_number",unique=false,nullable=false)
	public Long getAcFrom() {
		return acFrom;
	}

	public void setAcFrom(@NotNull @NotEmpty Long acFrom) {
		this.acFrom = acFrom;
	}
	

	@Column(name="to_acc_number",unique=false,nullable=false)
	public Long getAcTo() {
		return acTo;
	}

	public void setAcTo(@NotNull @NotEmpty Long acTo) {
		this.acTo = acTo;
	}

	@Column(name="txn_amt",unique=false,nullable=false)
	public double getTxnAmount() {
		return txnAmount;
	}

	public void setTxnAmount(@NotNull double txnAmount) {
		this.txnAmount = txnAmount;
	}

	@Column(name="txn_status",unique=false,nullable=false)
	public char getStatus() {
		return status;
	}

	public void setStatus(@NotNull char status) {
		this.status = status;
	}

	@Column(name="txn_create",unique=false,nullable=false)
	public Timestamp getTxnCreated() {
		return txnCreated;
	}

	public void setTxnCreated(Timestamp txnCreated) {
		this.txnCreated = txnCreated;
	}

	@Column(name="txn_update",unique=false,nullable=true)
	public Timestamp getTxnUpdated() {
		return txnUpdated;
	}

	public void setTxnUpdated(Timestamp txnUpdated) {
		this.txnUpdated = txnUpdated;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((acFrom == null) ? 0 : acFrom.hashCode());
		result = prime * result + ((acTo == null) ? 0 : acTo.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Transaction other = (Transaction) obj;
		if (acFrom == null) {
			if (other.acFrom != null)
				return false;
		} else if (!acFrom.equals(other.acFrom))
			return false;
		if (acTo == null) {
			if (other.acTo != null)
				return false;
		} else if (!acTo.equals(other.acTo))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	

	@Override
	public String toString() {
		return "Transaction [id=" + id + ", acFrom=" + acFrom + ", acTo="
				+ acTo + ", txnAmount=" + txnAmount + ", status=" + status
				+ ", txnCreated=" + txnCreated + ", txnUpdated=" + txnUpdated
				+ "]";
	}
}
