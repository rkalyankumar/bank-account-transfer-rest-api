package banktxnapp;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "account")
public class Account {
	private Long id;
	private String acHolderName;
	private double balance;
	private Timestamp lastTransacted;
	
	public Account() {
	}

	public Account(Long id, String acHolderName, double balance,
			long lastTransacted) {
		super();
		this.id = id;
		this.acHolderName = acHolderName;
		this.balance = balance;
		this.lastTransacted = new Timestamp(lastTransacted);
	}

	@Id
	@Column(name="account_number",unique=true,nullable=false)
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name="account_name",unique=false,nullable=false)
	public String getAcHolderName() {
		return acHolderName;
	}

	
	public void setAcHolderName(@NotNull @Max(255) String acHolderName) {
		this.acHolderName = acHolderName;
	}

	@Column(name="balance",unique=false,nullable=true)
	public double getBalance() {
		return balance;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}

	@Column(name="last_txn",unique=false,nullable=true)
	public Timestamp getLastTransacted() {
		return lastTransacted;
	}

	public void setLastTransacted(Timestamp lastTransacted) {
		this.lastTransacted = lastTransacted;
	}

	

	@Override
	public String toString() {
		return "Account [id=" + id + ", acHolderName=" + acHolderName
				+ ", balance=" + balance + ", lastTransacted=" + lastTransacted
				+ "]";
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Account that = (Account) obj;
		return this.id.compareTo(that.id) == 0;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((acHolderName == null) ? 0 : acHolderName.hashCode());
		long temp;
		temp = Double.doubleToLongBits(balance);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result
				+ ((lastTransacted == null) ? 0 : lastTransacted.hashCode());
		return result;
	}
}
