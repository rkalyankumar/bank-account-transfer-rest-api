package banktxnapp;

import java.sql.Timestamp;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
public class TransactionController {

	private static final Logger LOG = LoggerFactory.getLogger(TransactionController.class);
	
	@Autowired
	@Qualifier("transactionDao")
	private TransactionDao transactionDao;
	
	@Autowired
	@Qualifier("accountDao")
	private AccountDao accountDao;
	
	@RequestMapping(value="/transfer",method=RequestMethod.POST)
	public @ResponseBody Long transfer(@RequestBody TransferRequest transferRequest) {
		Transaction txn = new Transaction();
		txn.setAcFrom(transferRequest.getFromAccount());
		txn.setAcTo(transferRequest.getToAccount());
		txn.setTxnAmount(transferRequest.getAmount());
		txn.setStatus('P');
		txn.setTxnCreated(new Timestamp(System.currentTimeMillis()));
		transactionDao.create(txn);
		
		LOG.info("Transaction reference is: " + txn.getId());
		
		return txn.getId();
	}
	
	@RequestMapping(value="/transfer/{txnIdRef}",method=RequestMethod.PUT)
	public @ResponseBody Transaction complete(@PathVariable("txnIdRef") String txnIdRef) {
		LOG.info("txnIdRef:->->->: " + txnIdRef);
		Transaction txn = transactionDao.findPendingTransactionById(Long.valueOf(txnIdRef));
		if (txn == null) {
			throw new RuntimeException("Invalid transaction Reference: " + txnIdRef);
		}
		Account acFrom = accountDao.findById(txn.getAcFrom());
		Account acTo = accountDao.findById(txn.getAcTo());
		try {
			txn = transactionDao.transferAmount(acFrom, acTo, txn);
		} catch (Throwable t) {
			if (t instanceof InsufficientFundsException) {
				LOG.error(t.getLocalizedMessage(),t);
				// Transfer declined because of insufficient funds
				// the transaction should be completed with a "Failed"
				// state & shouldn't be open at any point of time.
				txn.setStatus('F');
				txn.setTxnUpdated(new Timestamp(System.currentTimeMillis()));
				transactionDao.update(txn);
			}
		}
		LOG.info(txn.toString());
		return txn;
	}
}
