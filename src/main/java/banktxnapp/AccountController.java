package banktxnapp;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class AccountController {

	private static final Logger LOG = LoggerFactory.getLogger(AccountController.class);
	
	@Autowired
	@Qualifier("accountDao")
	private AccountDao accountDao;
	
	@RequestMapping(value = "/account/greet",method=RequestMethod.GET)
	public @ResponseBody String defaultM() {
		return "Hello from service controller!";
	}
	
	@RequestMapping(value = "/account/new",method = RequestMethod.POST)
	public @ResponseBody Account create(HttpServletRequest request) {
		String acHolderName = request.getParameter("acHolderName");
		Double openingBal = Double.valueOf(request.getParameter("openingBal").trim());
		Account account = new Account();
		account.setAcHolderName(acHolderName);
		account.setBalance(openingBal);
		LOG.info("Creating account: " + account.toString());
		account = accountDao.save(account);
		LOG.info("Created account. Id = " + account.getId());
		return account;
	}
}
