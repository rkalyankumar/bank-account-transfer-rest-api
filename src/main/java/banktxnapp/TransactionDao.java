package banktxnapp;

import java.sql.Timestamp;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Component("transactionDao")
@Transactional
public class TransactionDao {

	private static final Logger LOG = LoggerFactory.getLogger(TransactionDao.class);
	
	private SessionFactory sessionFactory;
	
	@Autowired
	@Qualifier("accountDao")
	private AccountDao accountDao;
	
	@Autowired
	public TransactionDao(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	@Transactional(readOnly = false,propagation = Propagation.REQUIRED)
	public void create(Transaction transaction) {
		Session createTxn = sessionFactory.getCurrentSession();
		createTxn.save(transaction);
	}
	
	@Transactional(readOnly = false,propagation = Propagation.REQUIRED)
	public void update(Transaction transaction) {
		Session updateTxn = sessionFactory.getCurrentSession();
		updateTxn.merge(transaction);
	}
	
	@Transactional(readOnly = true,propagation = Propagation.SUPPORTS)
	public Transaction findById(Long id) {
		Transaction txn = null;
		Session session = sessionFactory.getCurrentSession();
		Query qry = session.createQuery("select t from Transaction t where t.id = :id");
		qry.setLong("id", id);
		txn = (Transaction) qry.uniqueResult();
		return txn;
	}

	@Transactional(readOnly = true,propagation = Propagation.SUPPORTS)
	public Transaction findPendingTransactionById(Long id) {
		// Used when looking up for pending transactions
		// This ensures that we pick the pending transactions for
		// completing the money transfer b/w accounts
		Transaction txn = null;
		Session session = sessionFactory.getCurrentSession();
		Query qry = session.createQuery("select t from Transaction t where t.id = :id and t.status= :status");
		qry.setLong("id", id);
		qry.setCharacter("status",'P');
		txn = (Transaction) qry.uniqueResult();
		return txn;
	}
	
	@Transactional(readOnly = false,propagation = Propagation.REQUIRED)
	public void remove(Transaction txn) {
		throw new UnsupportedOperationException("Transactions cannot be deleted!");
	}
	
	@Transactional(readOnly = false,propagation = Propagation.REQUIRED,rollbackForClassName="Exception,InsufficientFundsException,RuntimeException")
	public Transaction transferAmount(Account from,Account to,Transaction txnRef) {
		if (from.getBalance() - txnRef.getTxnAmount() < 0.0) {
			throw new InsufficientFundsException("Account # " 
					+ from.getId() + "[" + from.getAcHolderName() 
					+"] doesn't have sufficient funds to do this transfer!");
		}
		from.setLastTransacted(new Timestamp(System.currentTimeMillis()));
		from.setBalance(from.getBalance() - txnRef.getTxnAmount());
		to.setLastTransacted(from.getLastTransacted());
		to.setBalance(to.getBalance() + txnRef.getTxnAmount());
		txnRef.setStatus('S');
		txnRef.setTxnUpdated(to.getLastTransacted());
		accountDao.update(from);
		accountDao.update(to);
		this.update(txnRef);
		LOG.info("Transaction completed successfully!");
		return txnRef;
	}
	
}
