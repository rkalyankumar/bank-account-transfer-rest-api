package banktxnapp;



import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Component("accountDao")
@Transactional
public class AccountDao {
	private SessionFactory sessionFactory;
	private static final Logger LOG = LoggerFactory.getLogger(AccountDao.class);
	
	@Autowired 
	public AccountDao(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
		LOG.info("AccountDao created!!!");
	}
	
	@Transactional(readOnly = false,propagation = Propagation.REQUIRED)
	public Account save(Account account) {
		Session saveSession = sessionFactory.getCurrentSession();
		saveSession.save(account);
		return account;
	}
	
	@Transactional(readOnly = true,propagation = Propagation.SUPPORTS)
	public Account findById(Long id) {
		Account acc = null;
		Session session = sessionFactory.getCurrentSession();
		Query qry = session.createQuery("select a from Account a where a.id = :id");
		qry.setLong("id", id);
		acc = (Account) qry.uniqueResult();
		return acc;
	}
	
	@Transactional(readOnly = false,propagation = Propagation.REQUIRED)
	public void update(Account account) {
		Session updateSession = sessionFactory.getCurrentSession();
		updateSession.merge(account);
	}
	
	@Transactional(readOnly = false,propagation = Propagation.REQUIRED)
	public void remove(Account account) {
		Session deleteSession = sessionFactory.getCurrentSession();
		deleteSession.delete(account);
	}
}
