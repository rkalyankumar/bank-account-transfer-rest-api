-- Run following queries as root
create database actxndb;
create user 'accuser'@'localhost' identified by 'accpass';
grant all on actxndb.* to 'accuser'@'localhost' with grant option;

-- Logout of root user and login as accuser and switch to actxndb
-- database and run following create statements
create table account (
	account_number int auto_increment primary key,
	account_name varchar(256) not null,
	balance numeric(16,2),
	last_txn timestamp);

create table xferaccount (
	txnid int auto_increment primary key,
	from_acc_number int not null,
	to_acc_number int not null,
	txn_amt numeric(16,2) not null,
	txn_status char(1) not null,
	txn_create timestamp not null,
	txn_update timestamp
);

